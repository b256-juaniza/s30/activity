// [Section] MongoDB Aggregation
/*
  - Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
  - Compared to doing CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application.
*/

// Using the aggregate method
/*
  - Syntax
    - db.collectionName.aggregate([
      { $match: { fieldA, valueA } },
      { $group: { _id: "$fieldB" }, { result: { operation } } }
    ])
  - The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
*/

db.fruits.aggregate([
    // $match -> used to match or get documents that satisfies the condition
    // $match is similar to find(). Infact, you can even add query operators to make you criteria more flexible
    /*
        Syntax:
            { $match: {<field_name>: <value>}}
    */
    /*
        $match - Apple Banana Kiwi
    */
    { $match: {onSale: true}},
    // $group -> allows us to group together documents and create an analysis out of the group elements
    // _id: in the group stage, essentially associates an id to our results.
    // _id: also determines the number of groups
    // _id: "$<field_name>" -> group documents based on the value of the indicated field
    /*
        Apple - 1.0
        Banana - 2.0
        Kiwi - 1.0
    
        _id: 1.0
            Apple, Kiwi
            
            total: sum of the fruits stock
            total: Apple stocks + Kiwi stocks
            total:    20        +     25
            total: 45
    
        _id: 2.0
            Banana
            
            total: sum of the fruits stocks
            total: Banana stocks
            total: 15
    */
    // $sum -> used to add or total the values in a given field
    { $group: {_id: "$supplier_id", total: { $sum: "$stock"}}}
]);

db.fruits.aggregate([
    /*
      $match - Apple, Banana, Kiwi
    */
    { $match: {onSale: true}},
    /*
      $group - $supplier_id
          Apple - 1.0
          Banana - 2.0
          Kiwi - 1.0
      
      _id: 1.0
          
          $avg = (total sum of stocks)/ num of documents/fruits
          $avg = (Apple stocks + Kiwi stocks)/2
          $avg = (20 + 25)/2
          $avg = 45/2
          $avg = 22.5
  
      _id: 2.0
          
          $avg = total sum of stocks/num of fruits
          $avg = (Banana stocks)/1
          $avg = 15/1
          $avg = 15
    */
    // $avg -> gets the average of the values of the given field per group
    { $group: {_id:"$supplier_id", avgStocks: {$avg: "$stock"}}}
]);

db.fruits.aggregate([
    /*
        $match - Apple, Banana Kiwi
    */
    {$match: {onSale: true}},
    /*
        _id: fruitfarm
        
        $avg: (sum of fruits stocks)/number of fruits
        $avg: (Apple Stocks + Banana Stocks + Kiwi Stocks)/3
        $avg: (     20      +       15      +     25     )/3
        $avg: 60/3
        $avg: 20.0
    */
    {$group: {_id: "fruit_farm", avgStocks: {$avg: "$stock"}}}
])

db.fruits.aggregate([
    /*
        $match - Apple, Banana Kiwi
    */
    {$match: {onSale: true}},
    /*
        _id: null
        
        $avg: (sum of fruits stocks)/number of fruits
        $avg: (Apple Stocks + Banana Stocks + Kiwi Stocks)/3
        $avg: (     20      +       15      +     25     )/3
        $avg: 60/3
        $avg: 20.0
    */
    {$group: {_id: null, avgStocks: {$avg: "$stock"}}}
])

db.fruits.aggregate([
    // $match - Banana, Mango  
    {$match: {supplier_id: 2}},
    /*
        _id: highestPrice
        
        maxPrice:
            Banana - 20
            Mango - 120
    
        maxPrice: Mango
        maxPrice: 120
    */
    // $max -> allows us to get the highest value in a given field per group
    {$group: {_id: "highestPrice", maxPrice: {$max: "$price"}}}
])

db.fruits.aggregate([
    // $match - Apple, Banana, Kiwi
    {$match: {onSale: true}},
    /*
        $group
        
        _id: supplier_id
    
        minPrice: 
            Apple - 40
            Banana - 20
            Kiwi - 50
        
        minPrice: Banana
        minPrice: 20.0
    */
    // $min -> allow us to get the lowest value in a given field per group
    {$group: {_id: "supplier_id", minPrice: {$min: "$price"}}}
])

// $count - is a stage usually added after $match to count all items that matches our criteria.
db.fruits.aggregate([
    {$match: {$and: [
        {price: {$gt: 50}}, 
        {stock: {$lte: 20}}
    ]}},
    {$count: "itemsPriceGreaterThan50StocksLessThanorEqual20"}
])

// $out -> save/output the result in a new collection
// it is usually the last stage in an aggregation pipeline.
// Note: this will overwrite the collection if it already exists
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", totalStocks: {$sum: "$stock"}}},
    {$out: "stocksPerSupplier"}
]);
