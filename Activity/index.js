// Syntax for MongoDB
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },
  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },
  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },
  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "fruitsOnSale"},
    {$out: "fruitsOnSale"}
])

db.getCollection('fruitsOnSale').find({fruitsOnSale: 3},{_id: 0, fruitsOnSale: 1})

db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}},
    {$count: "enoughStock"},
    {$out: "enoughStock"}
])

db.getCollection('enoughStock').find({enoughStock: 2},{_id: 0, enoughStock: 1})

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id:"$supplier_id", avg_price: {$avg: "$price"}}},
    {$out: "avg_price"}
]);

db.getCollection('avg_price').find({})

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id:"$supplier_id", avg_price: {$avg: "$price"}}},
    {$out: "avg_price"}
]);

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {max_price: {$max: "$price"}, _id:"$supplier_id"}},
	{$out: "max_price"}
]);

db.getCollection('max_price').find({})

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {min_price: {$min: "$price"}, _id:"$supplier_id"}},
	{$out: "min_price"}
]);

db.getCollection('min_price').find({})
